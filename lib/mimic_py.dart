// mimics a Python (or Java/C#) style of coding for flutter

import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        AppBar appBar = AppBar(title: Text("Welcome to Flutter"));
        Center center = Center(child: Text("Hello World!"));
        Scaffold scaffold = Scaffold(appBar: appBar, body: center);

        Widget app = MaterialApp(
            title: 'Welcome to Flutter',
            home: scaffold,
        );
        return app;
    }
}

void main(){
    runApp(MyApp());
}

/*

Why isn't Flutter code written like this - style wise?

The build method is defining what is to be displayed, so it should read in a
format that best serves it. You almost have to read this style in reverse
to get a good picture but the original code camp code does it by default.

Approach it like a composed blueprint and not a set of ordered instructions.

@see https://www.reddit.com/r/FlutterDev/comments/9ei0dn/is_flutter_code_style_ugly/

for reference this is how it's written in the Flutter style

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Text('Hello World'),
        ),
      ),
    );
  }
}
 */